import subprocess
import time
import re

# Durée pendant laquelle le programme doit tourner (en secondes)
duree_execution = 15
temps_debut = time.time()

# Initialiser la liste des badges
liste_badges = []

while time.time() - temps_debut < duree_execution:
    try:
        # Appeler le programme nfctools.py avec la commande getuid
        result = subprocess.run(['python3', 'nfctool.py', 'getuid'], capture_output=True, text=True)
        
        # Vérifier si la commande a réussi
        if result.returncode == 0:
            # Extraire l'UID du retour de la commande en utilisant une expression régulière
            output = result.stdout
            match = re.search(r'getuid: ([0-9A-F ]+)', output)
            if match:
                uid = match.group(1).strip()
                if uid not in liste_badges:
                    liste_badges.append(uid)
                    print(f"Badge ajouté: {uid}")
                else:
                    print(f"Badge déjà présent: {uid}")
            else:
                print("UID non trouvé dans la sortie de la commande.")
        else:
            print(f"Erreur dans l'exécution de la commande: {result.stderr}")
        
        # Pause avant de relancer la commande
        time.sleep(1)
    
    except Exception as e:
        print(f"Erreur: {e}")

# Imprimer la liste des badges et leur nombre
print("Liste des badges:")
print(liste_badges)
print(f"Nombre de badges: {len(liste_badges)}")
